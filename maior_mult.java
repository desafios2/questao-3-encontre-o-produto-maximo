/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sources;

import java.util.ArrayList;

/**
 *
 * @author matheus
 */
public class maior_mult {

    public static ArrayList<Integer> check(ArrayList<Integer> arr) {
        //armazena os numeros positivos
        ArrayList<Integer> pos = new ArrayList<>();
        
        //armazena os numeros negativos
        ArrayList<Integer> neg = new ArrayList<>();

        //percorre o array separando os numeros em negativos e positivos(0 eh sempre ignorado)
        arr.forEach(i -> {
            if (i > 0) {
                pos.add(i);
            } else if (i < 0) {
                neg.add(i);
            }
        });
        
        //se a quantidade de numeros negativos em um array for impar: retirar o mais proximo de 0                                                              
        //se a quantidade for par a multiplicacao dos sinais sempre vai resultar em um numero positivo(pular o if)
        if(neg.size() % 2 != 0){
            Integer maior = Integer.MIN_VALUE;
            int index = 0;
            for(Integer i : neg){
                if(i > maior){
                    maior = i;
                    index = neg.indexOf(i);
                }
            }
            neg.remove(index);
        }
        
        //inserindo os numeros negativos na resposta(array de numeros positivos)
        pos.addAll(neg);

        return pos;
    }
}
